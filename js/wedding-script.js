//const websitePrefix = "james-laurie.wedding:8443"
const websitePrefix = "james-laurie.wedding:8443"
let inviteCode = loadInviteCode();

let invitation = null
let locale = null

// Main functions

function load() {
    loadData(function () {
        renderAll(true)
    });
}

function renderAll(forceReRender = false) {
    updateTextMappings()
    renderContent()
    if (invitation) {
        renderInvitation(forceReRender)
    } else {
        let message = inviteCode ? `No invitation matching invite code '${inviteCode}'. Please check and try again` : null
        renderInvitationIdForm($("#invitation"), message)
    }
}


function loadData(thenCall) {
    invitation = null
    loadLocale()
    if (inviteCode) {
        $.when(
            $.getJSON(`https://${websitePrefix}/invitations/${inviteCode}`, function (data) {
                invitation = data
                loadLocale()
            }),

        ).always(function () {
            thenCall()
        })
    } else {
        thenCall()
    }
}

function loadInviteCode() {
    let searchParams = new URLSearchParams(window.location.search)
    let inviteCode = searchParams.get("inviteCode")

    if (!inviteCode) {
        inviteCode = window.localStorage.getItem("inviteCode")
    } else if (inviteCode !== window.localStorage.getItem("inviteCode")) {
        // New invitation being loaded. Reset the locale
        window.localStorage.removeItem("locale")
        window.localStorage.setItem("inviteCode", inviteCode)
    }
    return inviteCode;
}

function loadLocale() {
    // Use previously set locale
    locale = window.localStorage.getItem("locale")
    if (!locale) {
        // else use invitation else use search param
        let searchParams = new URLSearchParams(window.location.search)
        locale = invitation ? invitation.locale : searchParams.get("locale")
        if (!locale) {
            locale =  "en-GB"
        }
    }
}

// Public functions
function loadFromInput() {
    inviteCode = $('#input-invite-code').val()
    window.localStorage.setItem("inviteCode", inviteCode)
    load()
    for (i = 1; i <7; i++) {
        delay(i * 100).then(() => location.hash = "#rsvp-header")
    }
}

function resetInvitationCode() {
    window.localStorage.removeItem("inviteCode")
    renderInvitationIdForm($("#invitation"))
}

function updateLocale() {
    let currLocal = $("#language-select").val()
    if (currLocal !== locale) {
        locale = currLocal
        window.localStorage.setItem("locale", locale)
        renderAll(true)
    }
}

// Content
function renderContent() {
    $('#language-select').val(locale)
    $("#header").html(`
      <h1 class="heading">
        James & Laurie
        <span class="heading-subtext"> ${getText("gettingMarried")}</span>
      </h1>
    `)
    $("#intro-section").html(`
        <h2 class="sub-heading">${getText("introHeading")}</h2>
        ${getText("introText")}
    `)
    $("#ceremony-section").html(`
        <i class="fas fa-calendar-check"></i>
        <h2 class="sub-heading">${getText("ceremonyLabel")}</h2>
        <div class="details">
            ${getText("ceremonyDetails")}
            <div class="map-responsive">
                <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2358.211498122984!2d3.8834440279308913!3d43.65696335127133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12b6a8b79a87f319%3A0x18945e3855f4bc30!2sMayor%20of%20Clapiers!5e0!3m2!1sen!2spt!4v1672496752262!5m2!1sen!2spt" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></p>
            </div>
            <p>
                ${getText("addToCalendar")}: <br/>
                <a href="https://calendar.google.com/calendar/render?action=TEMPLATE&dates=20230527T090000Z%2F20230527T214500Z&location=Clapiers%2C%20France&text=Laurie%20%26%20James%20-%20Wedding%20%2F%20Mariage" title="Save Event in my Calendar">Google</a> -
                <a href="https://outlook.live.com/calendar/0/deeplink/compose?allday=false&enddt=2023-05-27T21%3A45%3A00%2B00%3A00&location=Clapiers%2C%20France&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt=2023-05-27T09%3A00%3A00%2B00%3A00&subject=Laurie%20%26%20James%20-%20Wedding%20%2F%20Mariage" title="Save Event in my Calendar">Outlook</a> -
                <a href="https://outlook.office.com/calendar/0/deeplink/compose?allday=false&enddt=2023-05-27T21%3A45%3A00%2B00%3A00&location=Clapiers%2C%20France&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt=2023-05-27T09%3A00%3A00%2B00%3A00&subject=Laurie%20%26%20James%20-%20Wedding%20%2F%20Mariage" title="Save Event in my Calendar">Office 365</a>
            </p>
            <h4>Maison Gely</h4>
            <img class="resort-image" src="./img/Maison-Gely-Outside.jpg" alt="Maison Gely Outside" />
            <div class="map-responsive">
                <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2358.211498122984!2d3.8834440279308913!3d43.65696335127133!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa9aacc7376d3093e!2sLa%20Maison%20Gely!5e0!3m2!1sen!2spt!4v1672496787343!5m2!1sen!2spt" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></p>
            </div>
            ${getText("gettingHereFrance")}
            ${getText("whereToStayFrance")}
        </div>   
    `)

    $("#party-section").html(`
        <i class="fas fa-calendar-check"></i>
        <h2 class="sub-heading">${getText("receptionLabel")}</h2>
        <div class="details">
            ${getText("receptionDetails")}
            <h4>King Willian IV</h4>
            <img class="resort-image mb-2" src="./img/King-Bill-Outside.jpg" alt="King Bill Outside" />
            <img class="resort-image mb-2" src="./img/King-Bill-Inside.jpg" alt="King Bull Inside" />
            <div class="map-responsive">
                <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2454.3284472386717!2d0.08261891648972562!3d52.03733067972573!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d87e0de19bcfd9%3A0x765181970e39c643!2sKing%20William%20IV%20Heydon!5e0!3m2!1sen!2spt!4v1672496818607!5m2!1sen!2spt" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></p>
            </div>
            <p>
                ${getText("addToCalendar")}: <br/>
                <a href="https://calendar.google.com/calendar/render?action=TEMPLATE&dates=20230603T170000Z%2F20230603T230000Z&location=Heydon%2C%20United%20Kingdom&text=Laurie%20%26%20James%20-%20Reception%20%2F%20R%C3%A9ception" title="Save Event in my Calendar">Google</a> -
                <a href="https://outlook.live.com/calendar/0/deeplink/compose?allday=false&enddt=2023-06-03T23%3A00%3A00%2B00%3A00&location=Heydon%2C%20United%20Kingdom&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt=2023-06-03T17%3A00%3A00%2B00%3A00&subject=Laurie%20%26%20James%20-%20Reception%20%2F%20R%C3%A9ception" title="Save Event in my Calendar">Outlook</a> -
                <a href="https://outlook.office.com/calendar/0/deeplink/compose?allday=false&enddt=2023-06-03T23%3A00%3A00%2B00%3A00&location=Heydon%2C%20United%20Kingdom&path=%2Fcalendar%2Faction%2Fcompose&rru=addevent&startdt=2023-06-03T17%3A00%3A00%2B00%3A00&subject=Laurie%20%26%20James%20-%20Reception%20%2F%20R%C3%A9ception" title="Save Event in my Calendar">Office 365</a>
            </p>
            ${getText("gettingHereEngland")}
            ${getText("whereToStayEngland")}
            <div class="map-responsive">
                <p><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2450.674026313481!2d0.16418351649114005!3d52.103863879737574!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47d87c9f98bdca33%3A0x804b02b6b107efe8!2sRed%20Lion%20Hotel!5e0!3m2!1sen!2spt!4v1672496852605!5m2!1sen!2spt" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe></p>
            </div>
        </div>  
        ${getText("giftsSection")}
    `)
    $("#rsvp-header").html(`
      <i class="fas fa-reply"></i>
      <h2 class="sub-heading" id="rsvp">RSVP</h2>
    `)
}

// Invitation Form
function renderInvitationIdForm(targetDiv, errorMessage = null) {
    targetDiv.removeClass("invitation")
    targetDiv.html(`
        <div class="input-group mb-3 invite-id-input">
          <input type="text" class="form-control" id="input-invite-code" placeholder="${getText("inviteCode")}" aria-label="Invite Code" aria-describedby="btn-invite-code">
          <button class="btn btn-outline-primary" type="button" id="btn-invite-code" onclick="loadFromInput()">${getText("submitButton")}</button>
        </div>
        ${errorMessage ? `<div class="alert alert-danger" role="alert">${errorMessage}</div>` : ``}
    `)
}

// Invitation functions
function renderInvitation(forceReRender = false) {
    if (forceReRender || $(`#invitation-text`).length === 0) {
        let invitationDiv = $("#invitation")
        invitationDiv.addClass("invitation")
        let names = invitation.invitees.map( i => i.name.split(" ")[0])
        console.log(names)
        let prettyNames = names.length > 1 ? names.slice(0, names.length - 1).join(", ") + ` ${getText("and")} ${names[names.length -1]}` : names[0]
        invitationDiv.html(`
            <div class="invitation-text" id="invitation-text">
                <span class="tiny-link"><a onclick="resetInvitationCode()" href="javascript:void(0);">${getText("loadNewInvitation")}</a></span> 
                <h4>${getText("greeting")} ${prettyNames},</h4>
                ${getText("letterContent")}
                <h4>Jamie and Laurie</h4>
            </div>
            <div class="" id="nav-response" role="tabpanel" aria-labelledby="nav-home-tab"
                 tabindex="0"></div>
            <div class="invitation-footer"></div>
        `)
    }

    // Render RSVP Section
    renderRSVPTab($("#nav-response"))
}


// RSVP Tab
function renderRSVPTab(targetDiv) {

    if ($(`#response-accordion`).length === 0) {
        targetDiv.html(`
            <div class="with-padding">${getText("rsvpIntro")}</div>
            <div class="accordion" id="response-accordion"></div>
            <div id="rsvp-saving-div" class="mt-2 mb-2 d-grid gap-2 d-md-flex justify-content-md-end">
                <button class="btn btn-success" onclick="saveRSVPs()">${getText("save")}</button>
            </div>
        `)
    }
    invitation.invitees.forEach((invitee) => {
        const responseAccordion = $("#response-accordion")
        if ($(`#collapse-${invitee.id}`).length === 0) {
            renderAccordion(responseAccordion, invitee.id, invitee.name)
        }
        renderRSVPForm(invitee)
    });
}


function renderAccordion(targetDiv, id, label) {
    targetDiv.append(`<div class="accordion-item">
        <h2 class="accordion-header" id="heading-${id}">
          <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse-${id}" aria-expanded="true" aria-controls="collapse-${id}">
            ${label}
          </button>
        </h2>
        <div id="collapse-${id}" class="accordion-collapse container show" aria-labelledby="heading-${id}">
        </div>
    </div>`)
    return $(`#collapse-${id}`)
}

function renderRSVPForm(invitee) {
    let targetDiv = $(`#collapse-${invitee.id}`)

    let usingPlusOne = invitee.plusOne != null
    let guestName = usingPlusOne ? invitee.plusOne.name : ""

    targetDiv.html(`<form class="gx-5">
        ${invitation.ceremony ? `
        <div class="btn-toolbar mt-3 mb-1 justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
            <div class="col-7">${getText("attendingCeremonyLabel")}</div>
            <div class="btn-group me-2" role="group" aria-label="First group">
                <input type="radio" class="btn-check" name="attending-ceremony-options-${invitee.id}" id="btn-attending-ceremony-yes-${invitee.id}" 
                    autocomplete="off"${invitee.attendingCeremony ? " checked" : ""} onchange="updateAttending('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-attending-ceremony-yes-${invitee.id}">${getText("yes")}</label>
       
                <input type="radio" class="btn-check" name="attending-ceremony-options-${invitee.id}" id="btn-attending-ceremony-no-${invitee.id}" 
                    autocomplete="off"${invitee.attendingCeremony ? "" : " checked"} onchange="updateAttending('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-attending-ceremony-no-${invitee.id}">${getText("no")}</label>
            </div>
        </div>     
            
            ${invitee.attendingCeremony ? `
        <div class="btn-toolbar mt-1 mb-3 justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
            <div class="col-7">${getText("minibusRequiredLabelCeremony")}</div>
            <div class="btn-group me-2" role="group" aria-label="First group">
                <input type="radio" class="btn-check" name="minibus-ceremony-options-${invitee.id}" id="btn-minibus-ceremony-yes-${invitee.id}" 
                    autocomplete="off"${invitee.minibusCeremony ? " checked" : ""} onchange="updateAttending('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-minibus-ceremony-yes-${invitee.id}">${getText("yes")}</label>
       
                <input type="radio" class="btn-check" name="minibus-ceremony-options-${invitee.id}" id="btn-minibus-ceremony-no-${invitee.id}" 
                    autocomplete="off"${invitee.minibusCeremony ? "" : " checked"} onchange="updateAttending('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-minibus-ceremony-no-${invitee.id}">${getText("no")}</label>
            </div>
        </div>     
            ` : ``}
        ` : ``}
        ${invitation.reception ? `
        <div class="btn-toolbar mt-3 mb-1 justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
            <div class="col-7">${getText("attendingReceptionLabel")}</div>
            <div class="btn-group me-2" role="group" aria-label="First group">
                <input type="radio" class="btn-check" name="attending-reception-options-${invitee.id}" id="btn-attending-reception-yes-${invitee.id}" 
                    autocomplete="off"${invitee.attendingReception ? " checked" : ""} onchange="updateAttending('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-attending-reception-yes-${invitee.id}">${getText("yes")}</label>
       
                <input type="radio" class="btn-check" name="attending-reception-options-${invitee.id}" id="btn-attending-reception-no-${invitee.id}" 
                    autocomplete="off"${invitee.attendingReception ? "" : " checked"} onchange="updateAttending('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-attending-reception-no-${invitee.id}">${getText("no")}</label>
            </div>
        </div>     
            ${invitee.attendingReception ? `
        <div class="btn-toolbar mt-1 mb-3 justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
            <div class="col-7" >${getText("minibusRequiredLabelReception")}</div>
            <div class="btn-group me-2" role="group" aria-label="First group">
                <input type="radio" class="btn-check" name="minibus-reception-options-${invitee.id}" id="btn-minibus-reception-yes-${invitee.id}" 
                    autocomplete="off"${invitee.minibusReception ? " checked" : ""} onchange="updateAttending('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-minibus-reception-yes-${invitee.id}">${getText("yes")}</label>
       
                <input type="radio" class="btn-check" name="minibus-reception-options-${invitee.id}" id="btn-minibus-reception-no-${invitee.id}" 
                    autocomplete="off"${invitee.minibusReception ? "" : " checked"} onchange="updateAttending('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-minibus-reception-no-${invitee.id}">${getText("no")}</label>
            </div>
            ` : ``}
        </div>     
        ` : ``}
        ${invitee.eligibleForPlusOne && (invitee.attendingReception || invitee.attendingCeremony) ? `
        <div class="btn-toolbar mb-3 justify-content-between" role="toolbar" aria-label="Toolbar with button groups">
            <div class="col-7">${getText("usingPlusOneLabel")}</div>
                <div class="btn-group me-2" role="group" aria-label="First group">
                <input type="radio" class="btn-check" name="guest-options-${invitee.id}" id="btn-guest-yes-${invitee.id}" 
                    autocomplete="off"${usingPlusOne ? " checked" : ""} onchange="updateGuest('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-guest-yes-${invitee.id}">${getText("yes")}</label>
       
                <input type="radio" class="btn-check" name="guest-options-${invitee.id}" id="btn-guest-no-${invitee.id}" 
                    autocomplete="off"${usingPlusOne ? "" : " checked"} onchange="updateGuest('${invitee.id}')">
                <label class="btn btn-outline-navy" for="btn-guest-no-${invitee.id}">${getText("no")}</label>
            </div>
        </div>
        <fieldset class="mb-2" ${!usingPlusOne ? "disabled" : ""} onchange="updateGuestName('${invitee.id}')">
            <div class="input-group">
            <span class="input-group-text">${getText("plusOneLabel")}</span>
                <div class="form-floating">
                    <input type="text" class="form-control" id="name-input-${invitee.id}" value="${guestName}">
                    <label for="name-input-${invitee.id}">${getText("nameLabel")}</label>
                </div>
            </div>
        </fieldset>
        ` : ""}
        <div class="mb-3">
            <label for="notes-${invitee.id}" class="form-label">${getText("noteLabel")}</label>
            <textarea class="form-control" id="notes-${invitee.id}" rows="2" onchange="updateNote('${invitee.id}')">${invitee.notes}</textarea>
        </div>
    </form>`)
}

function findInvitee(inviteeId) {
    return invitation.invitees.filter(function (item) {
        return item.id === inviteeId;
    })[0]
}

function updateNote(inviteeId) {
    let invitee = findInvitee(inviteeId)

    invitee.notes = $(`#notes-${invitee.id}`).val()
}

function updateAttending(inviteeId) {
    let invitee = findInvitee(inviteeId)

    invitee.attendingCeremony = $(`input[name=attending-ceremony-options-${invitee.id}]:checked`).attr('id') === `btn-attending-ceremony-yes-${invitee.id}`
    invitee.attendingReception = $(`input[name=attending-reception-options-${invitee.id}]:checked`).attr('id') === `btn-attending-reception-yes-${invitee.id}`
    invitee.minibusCeremony = invitee.attendingCeremony && $(`input[name=minibus-ceremony-options-${invitee.id}]:checked`).attr('id') === `btn-minibus-ceremony-yes-${invitee.id}`
    invitee.minibusReception = invitee.attendingReception && $(`input[name=minibus-reception-options-${invitee.id}]:checked`).attr('id') === `btn-minibus-reception-yes-${invitee.id}`

    console.log(invitee.minibusReception)
    console.log(invitee.minibusCeremony)

    if (invitee.attendingCeremony !== true && invitee.attendingReception !== true) {
        invitee.plusOne = null
    }

    renderRSVPForm(invitee)
}

function updateGuestName(inviteeId) {
    let invitee = findInvitee(inviteeId)

    invitee.plusOne = {
        name: $(`#name-input-${invitee.id}`).val()
    }
}

function updateGuest(inviteeId) {
    const invitee = findInvitee(inviteeId)

    let selectedId = $(`input[name=guest-options-${invitee.id}]:checked`).attr('id')

    let usingPlusOne = selectedId === `btn-guest-yes-${invitee.id}`

    if (usingPlusOne && invitee.plusOne == null) {
        invitee.plusOne = {
            name: ""
        }
    }
    if (!usingPlusOne) {
        invitee.plusOne = null
    }

    renderRSVPForm(invitee)
}

function saveRSVPs() {
    renderSaveInProgress($("#rsvp-saving-div"))
    let updates = []
    invitation.invitees.forEach((invitee) => {
        updates.push({
            id: invitee.id,
            guest: invitee.plusOne,
            attendingCeremony: invitee.attendingCeremony,
            attendingReception: invitee.attendingReception,
            ceremonyMinibus: invitee.minibusCeremony,
            receptionMinibus: invitee.minibusReception,

            notes: invitee.notes
        })
    })
    $.ajax({
        contentType: 'application/json',
        data: JSON.stringify(updates),
        dataType: 'json',
        url: `https://${websitePrefix}/invitations/` + inviteCode,
        type: 'PUT'
    }).done(function (result) {
        invitation = result
        renderInvitation()
        delay(1000).then(() => {
            renderSaveWithText($("#rsvp-saving-div"), "✅ ", getText("saved"))
        })
    }).fail(function (_) {
        delay(1000).then(() => renderSaveWithText($("#rsvp-saving-div"),  "❌ ", getText("save"), "saveRSVPs"))
    });
}

// Helpers

function delay(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

function renderSaveInProgress(targetDiv) {
    targetDiv.html(`
        <button class="btn btn-success" disabled>
            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            ${getText("saving")}...
        </button>  
    `)
}

function renderSaveWithText(targetDiv, prefix, text, targetFun = null) {
    targetDiv.html(`
        <button class="btn btn-secondary" ${targetFun ? `onclick="${targetFun}()"` : `disabled`} >${prefix} ${text}</button>
    `)
    delay(10_000).then(() => targetDiv.html(`
        <button class="btn btn-secondary" ${targetFun ? `onclick="${targetFun}()"` : `disabled`}">${text}</button>
    `))
}


// Localisation
function getText(id) {
    return textMapping[id][locale]
}


function getCeremonyTime() {
    const ceremonyDate = new Date(Date.UTC(2023, 4, 27, 9));
    let options = {
        weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',
        hour: 'numeric', minute: 'numeric', timeZone: 'Europe/Paris',
    };
    return new Intl.DateTimeFormat(locale, options).format(ceremonyDate);
}

function getReceptionTime() {
    const partyDate = new Date(Date.UTC(2023, 5, 3, 17))
    let options = {
        weekday: 'long', year: 'numeric', month: 'long', day: 'numeric',
        hour: 'numeric', minute: 'numeric', timeZone: 'Europe/London',
    };
    return new Intl.DateTimeFormat(locale, options).format(partyDate);
}

function updateTextMappings() {
    textMapping = {
        "gettingMarried": {
            "en-GB": "are getting married!",
            "fr-FR": "se disent oui !"
        },
        "introHeading": {
            "en-GB": "Our Wedding",
            "fr-FR": "Notre mariage"
        },
        "introText": {
            "en-GB": `
            <p>We're very excited to announce that we will be getting married this year!</p>
            <p>We'll be having a ceremony in France with our families followed by the reception in England the following weekend.</p>
            <p>We hope you will be able to join us to celebrate!</p>`,
            "fr-FR": `
            <p>Nous avons le plaisir de vous convier à notre mariage dans le sud de la France.</p>
            Celui-ci s organisera en deux temps : 
            <ul>
            <li>Le mariage civil aura lieu à la mairie de Clapiers</li>
            <li>La réception ainsi que notre première danse en Angleterre</li>
            </ul>
            <p>Nous vous attendons pour une magnifique journée.</p>`
        },
        "ceremonyDetails": {
            "en-GB": `
            <p>
            We are having our wedding ceremony at Clapiers City Hall followed by dinner at Maison Gely with close family 
            and select friends.<br> 
            Numbers are limited so as much as we'd love to have everyone, unfortunately it will not be possible. <br>
            If your invitation doesn't mention the ceremony, don't worry, look further down for the information on the 
            Wedding Reception the following weekend.
            </p>
            <p>
            <b>The ceremony will be at ${getCeremonyTime()}.</b><br>
            Please arrive 15 minutes before the start of the ceremony.
            </p>
            <h4>Clapiers City Hall</h4>
            <p>
            The city hall is in the centre of Clapiers. <br> 
            If you are driving there is plenty of parking near Maison Gely which is just a 5 minute walk away (see below for location).
            </p>`,
            "fr-FR": `
            <p><b>${getCeremonyTime()}.</b><br></p>
            <h4>Clapiers Hotel de Ville</h4>
            `
        },
        "gettingHereFrance": {
            "en-GB": `
            <h4>How to get here</h4>
            <p>
            The closest airport is <b>Montpellier</b>. From the airport to the city there is a regular shuttle bus<br>
            </p>
            <p>
            Other options: 
            <ul>
                <li>Nîmes - very few flights, but simple transfer to station then a 40-minute train to Montpellier</li>
                <li>Marseille - many flights, but further away. About 2 hours bus or train to Montpellier</li>
                <li>Beziers - not too far, never taken this flight myself so unclear on connections</li>    
            </ul>
            </p>
            <p>
            From Montpellier to Clapiers we'll provide a minibus, however if you want to make your own way we'd recommend
            a taxi. The closest tramway is Jacou on Tramway 2, but it will still leave you a fair way outside Clapiers. 
            </p>`,
            "fr-FR": ``
        },
        "whereToStayFrance": {
            "en-GB": `
            <h4>Where to stay</h4>
            <p>
            There is a camping village located near the town you can find details <a href="https://www.sandaya.fr/nos-campings/plein-air-des-chenes">here.</a><br>
            Alternatively there are many hotels in the centre of Montpellier. <br>
            We will provide a minibus between Montpellier centre and Clapiers for those staying in the city.
            </p>`,
            "fr-FR": ""
        },

        "ceremonyLabel": {
            "en-GB": "The Ceremony",
            "fr-FR": "La Cérémonie"
        },
        "receptionLabel": {
            "en-GB": "The Wedding Reception",
            "fr-FR": "La réception"
        },
        "receptionDetails": {
            "en-GB": `
            <p>
            We will be having our Wedding Reception at the King William IV Pub in Heydon. There will be speeches and our 
            First Dance. Followed by live music into the evening with a buffet provided. 
            </p>
            <p>
            <b>The reception will begin at ${getReceptionTime()} with carraiges at midnight.</b><br>
            </p>`,
            "fr-FR": `
            <p>
            La réception du mariage aura lieu au Pub King William IV à Heydon. 
            </p>
            <p>
            La fête commencera ${getReceptionTime()}.<br>   
            </p>`
        },
        "giftsSection": {
            "en-GB": ``,
            "fr-FR": `
                <section class="section" id="gift-section">
                    <h4 class="sub-heading">Liste de Mariage</h4>
                    <p>
                        Certains nous l'ont demandé : Une liste de mariage est disponible à l'agence de voyage Le Monde à 
                        la Carte. Si vous le souhaitez vous pouvez les contacter via ce numéro de téléphone 
                        :04 67 61 18 00 ou cette adresse email : 
                        <a href="mailto:agence@lemondealacarte.com">agence@lemondealacarte.com</a> 
                        Nous sommes impatient de vous retrouver pour ce grand jour.
                    </p>
                </section>
            `
        },
        "gettingHereEngland": {
            "en-GB": `
            <h4>How to get here</h4>
            <p>
            London has several airports. We would recommend <b>London Stansted</b> though as it is the closest with the most 
            direct routes.
            </p>
            <p>
            The pub itself has parking if you are driving. <br> 
            We'll be arranging a minibus from <b>Whittlesford Parkway</b> station to the Pub. <br>
            The best option besides these will be getting a taxi from one of the nearby stations:
            <ul>
                <li><b>Royston</b> - London Kings Cross/Cambridge Line</li>
                <li><b>Whittlesford Parkway</b> - Stansted/Cambridge Line or London Liverpool Street/Cambridge line</li>
                <li><b>Audley End</b> - Stansted/Cambridge line or London Liverpool Street/Cambridge line</li>
            </ul>
            </p>`,
            "fr-FR": `
            <h4>Comment se rendre au Pub ?</h4>
             <p>
             Londres possède plusieurs aéroports. Le plus proches étant <b>Londres Stansted</b>.
             </p>
             <p>
             Le pub dispose d'un parking si vous conduisez. <br>
             Une navette sera mise à disposition depuis la station <b>Whittlesford Parkway</b>. <br>
             La meilleure option en plus de la navette sera de prendre un taxi depuis l'une des gares à proximité :
             <ul>
                 <li><b>Royston</b> – ligne Londres Kings Cross/Cambridge</li>
                 <li><b>Whittlesford Parkway</b> - ligne Stansted/Cambridge ou ligne London Liverpool Street/Cambridge</li>
                 <li><b>Audley End</b> – ligne Stansted/Cambridge ou ligne London Liverpool Street/Cambridge</li>
             </ul>
             </p>`
        },

        "whereToStayEngland": {
            "en-GB": `
            <h4>Where to Stay</h4>
            <p>
            We would recommend this hotel in Whittlesford (the Red Lion). <br>
            It is the closest station to the Pub, and we will be providing transportation between the hotel and the pub <br>
            You can book through their <a href="https://www.redlionwhittlesfordbridge.com/">website</a>
            </p>
            <p>
            Alternatively Cambridge has lots of choices for accommodation, but you'll need to make your own way to either
            Whittlesford Parkway station or the Pub itself
            </p>`,
            "fr-FR": `
            <h4>Hébergements</h4>
            <p>
             Nous recommandons l’hôtel « The Red Lion » à Whittlesford. <br>
             Vous pouvez réserver via leur <a href="https://www.redlionwhittlesfordbridge.com/">site Web</a>
             </p>
             <p>
             Si vous préférez, Cambridge propose de nombreux choix d'hébergement (Airbnb, hôtel, bed & breakfast).
             </p>
             <p>
             Cependant vous devrez vous rendre par vos propres moyens à la Station Whittlesford Parkway ou au pub lui-même.
             </p>
            `
        },
        "addToCalendar": {
            "en-GB": "Add to Calendar!",
            "fr-FR": "Ajouter au calendrier!"
        },
        "rsvpHeading": {
            "en-GB": "RSVP",
            "fr-FR": "RSVP"
        },
        "submitButton": {
            "en-GB": "Submit",
            "fr-FR": "Envoyer"
        },
        "inviteCode": {
            "en-GB": "Invite Code",
            "fr-FR": "Code invité"
        },
        "greeting":  {
            "en-GB": "Dear",
            "fr-FR": "Cher"
        },
        "family":  {
            "en-GB": "family",
            "fr-FR": "famille"
        },
        "letterContent":  {
            "en-GB": `
            <p>
            We really hope you will be able to join us for our wedding day. We ask that you please complete the 
            forms below to let us know if you will be attending.
            </p>
            <p>
            As mentioned above we are providing transportation between the events and the accommodatiom. Please let us 
            know if this is needed.
            </p>`,
            "fr-FR": `
            <p>
            Nous espérons que vous serez avec nous le jour de notre mariage. Pourriez-vous remplir le formulaire ci-dessous.
            </p>
            <p>
            Comme expliqué un peu plus haut nous mettons en place une navette pour les personnes qui logent à l'hôtel "The red Lion".
            </p>`
        },
        "rsvpIntro":  {
            "en-GB": "Please let us know who will be joining us!",
            "fr-FR": "Serez-vous avec nous pour ce grand jour ?"
        },
        "save":  {
            "en-GB": "Save",
            "fr-FR": "Sauvegarder"
        },
        "saving":  {
            "en-GB": "Saving",
            "fr-FR": "Sauvegarde"
        },
        "saved":  {
            "en-GB": "Saved",
            "fr-FR": "Enregistré"
        },
        "yes":  {
            "en-GB": "Yes",
            "fr-FR": "Oui"
        },
        "no":  {
            "en-GB": "No",
            "fr-FR": "Non"
        },
        "and":  {
            "en-GB": "and",
            "fr-FR": "et"
        },
        "attendingCeremonyLabel":  {
            "en-GB": "Attending Ceremony in France?",
            "fr-FR": "Présent pour la Cérémonie en France ?"
        },
        "attendingReceptionLabel":  {
            "en-GB": "Attending Reception in England?",
            "fr-FR": "Présent pour la réception en Angleterre ?"
        },
        "minibusRequiredLabelCeremony":  {
            "en-GB": "Will you need a minibus for the ceremony?",
            "fr-FR": "Besoin de Bus pour la Cérémonie ?"
        },
        "minibusRequiredLabelReception":  {
            "en-GB": "Will you need a minibus for the reception?",
            "fr-FR": "Besoin de Bus pour la réception ?"
        },
        "usingPlusOneLabel":  {
            "en-GB": "Using Plus One?",
            "fr-FR": "Plus un ?"
        },
        "plusOneLabel":  {
            "en-GB": "Plus One",
            "fr-FR": "Plus un"
        },
        "nameLabel":  {
            "en-GB": "Name",
            "fr-FR": "Nom"
        },
        "noteLabel":  {
            "en-GB": "Dietary Requirement / Special Requests",
            "fr-FR": "Exigence alimentaire / Demandes spéciales"
        },
        "loadNewInvitation":  {
            "en-GB": "Load a different invitation",
            "fr-FR": "Charger une autre invitation"
        },
        "workInProcess": {
            "en-GB": "We are still preparing invitations, please come back once you have yours",
            "fr-FR": "Nous préparons toujours les invitations, merci de revenir une fois que vous aurez les vôtres"
        }

    }
}

let textMapping = null
load()